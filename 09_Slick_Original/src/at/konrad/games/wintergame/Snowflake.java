package at.konrad.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import java.util.Random;

public class Snowflake implements Actor {
	private float x, y;
	private float size;
	private float speed;

	public Snowflake(String size) {
		super();
		setRandomXYValues();
		setDefaultValues(size);
	}

	private void setDefaultValues(String size) {
		if (size.equals("big")) {
			this.size = 20;
			this.speed = (float) 1;
		} else if (size.equals("medium")) {
			this.size = 15;
			this.speed = (float) 0.75;
		} else if (size.equals("small")) {
			this.size = 10;
			this.speed = (float) 0.5;
		}
	}

	private void setRandomXYValues() {
		this.x = getRandomNumber(800);
		this.y = getRandomNumber(600) * -1;
	}

	private int getRandomNumber(int range) {
		Random r = new Random();
		return r.nextInt(range);
	}

	public void update(GameContainer gc,int delta) {

		if (y < 700) {
			this.y = this.y + this.speed;
		} else {
			setRandomXYValues();
		}

	}

	public void render(Graphics graphics) {
		graphics.fillOval((float) this.x, (float) this.y, this.size, this.size);

	}

}

package at.konrad.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import java.util.Random;

public class Shootingstar implements Actor {

	public float x, y, w;
	public boolean isVisible;
	public int CountingTime;

	public Shootingstar(int x,int y) {
		super();
		this.x = x;
		this.y = y;
		this.isVisible = false;
		CountingTime = 0;
	}






	public void update(GameContainer gc, int delta) {

		this.CountingTime += delta;
		
		System.out.println(this.CountingTime);
		System.out.println(this.w);
		
		if (this.CountingTime > this.w && this.CountingTime  <  this.w + 5000) {

			this.isVisible = true;
			this.x++;
			this.y = (float) (0.0005 * this.x * this.x + -0.001 * this.x + 300);
			
		} else if (this.CountingTime > this.w + 4999 ) {
			this.isVisible = false;
			this.CountingTime = 0;
			this.x = -30;
			this.y = 100;
			setRandomTValues();

		}
	}

	private void setRandomTValues() {
		 this.w = getRandomNumber(10000) + 5000;
		 

	}

	private int getRandomNumber(int range) {
		Random r = new Random();
		return r.nextInt(range);
	}

	public void render(Graphics graphics) {
		if (isVisible == true) {
			graphics.fillOval(this.x, this.y, 20, 20, 4);

		}
	}
}

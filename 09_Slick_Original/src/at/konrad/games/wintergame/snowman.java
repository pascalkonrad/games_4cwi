package at.konrad.games.wintergame;

import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.util.Log;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class snowman implements Actor {

	private float x, y, xb, yb;
	private boolean keyescape, keydown, keyup, keyspace, b;
	private Image image;
	private int ball;

	snowman() throws SlickException {
		this.image = new Image("testdata/snowman.png");
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		this.keyescape = gc.getInput().isKeyDown(Input.KEY_ESCAPE);
		this.keyup = gc.getInput().isKeyDown(Input.KEY_UP);
		this.keydown = gc.getInput().isKeyDown(Input.KEY_DOWN);
		this.keyspace = gc.getInput().isKeyDown(Input.KEY_SPACE);
		if (this.keydown == true) {
			this.y++;
		} else if (this.keyup == true) {
			this.y--;
		} else if (this.keyescape == true) {
			System.exit(0);
		} else if (this.keyspace == true) {
			this.b = true;
			this.xb = this.x;
			this.yb = this.y;
		}

		 if (this.b == true && this.xb < 850) {
			shoot(delta);

		} else if (this.xb > 850) {
			this.b = false;
			

		}
	}

	private void shoot(int ball) {

		this.xb++;

	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.drawImage(image, x, y);
		graphics.fillOval(this.xb, this.yb = this.y, 10, 10);

	}

}

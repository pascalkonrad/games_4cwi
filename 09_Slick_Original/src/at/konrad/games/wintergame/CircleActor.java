package at.konrad.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class CircleActor implements Actor {

	public float x, y;

	public void update(GameContainer gc, int delta) {
		if (this.y < 600) {
			this.y++;

		} else if (this.y == 600) {
			this.y = -50;

		}
	}

	public void render(Graphics graphics) {

		graphics.drawOval(this.x, this.y, 50, 50);
	}

	public CircleActor(float x, float y) {
		super();
		this.x = x;
		this.y = y;
	}

	

}

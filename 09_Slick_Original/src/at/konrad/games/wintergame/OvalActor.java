package at.konrad.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class OvalActor implements Actor {
	private float x, y;
	private boolean circleRight = true;
	private boolean circleLeft = false;

	public void render(Graphics graphics) {

		graphics.drawOval(this.x, this.y, 70, 50);
	}

	public OvalActor(float x, float y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void update(GameContainer gc, int delta) {
		if (this.circleRight == true) {
			this.x++;
			if (this.x == 550) {
				this.circleRight = false;
				this.circleLeft = true;

			}
		} else if (this.circleLeft == true) {
			this.x--;
			if (this.x == 50) {
				this.circleRight = true;
				this.circleLeft = false;

			}

		}

	}
	
}

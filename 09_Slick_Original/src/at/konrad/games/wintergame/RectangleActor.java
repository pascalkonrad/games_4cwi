package at.konrad.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectangleActor implements Actor {

	public float x, y;
	private boolean rectUp = false;
	private boolean rectDown = true;
	private boolean rectLeft = false;
	private boolean rectRight = true;

	public RectangleActor(float x, float y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void update(GameContainer gc,int delta) {
		if (this.y < 600) {
			if (this.rectRight == true) {
				this.x++;
				if (this.x == 650) {
					this.rectRight = false;
					this.rectDown = true;
				} else {
					this.rectRight = true;
				}

			} else if (this.rectDown == true) {
				this.y++;
				if (this.y == 450) {
					this.rectDown = false;
					this.rectLeft = true;
				} else {
					this.rectDown = true;
				}

			} else if (this.rectLeft == true) {
				this.x--;
				if (this.x == 20) {
					this.rectLeft = false;
					this.rectUp = true;
				} else {
					this.rectLeft = true;
				}

			} else if (this.rectUp == true) {
				this.y--;
				if (this.y == 50) {
					this.rectUp = false;
					this.rectRight = true;
				} else {
					this.rectUp = true;
				}

			}
		}

	}

	public void render(Graphics graphics) {

		graphics.drawRect(this.x, this.y, 50, 50);
	}

}

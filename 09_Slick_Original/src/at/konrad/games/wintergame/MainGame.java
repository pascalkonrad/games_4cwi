package at.konrad.games.wintergame;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame {
	   private List <Actor> actors;

	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// TODO Auto-generated method stub
		for (Actor actors : this.actors ) {
			actors.render(graphics);
			
		}
	
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		this.actors = new ArrayList<>();
		this.actors.add(new CircleActor(200, 100));
		this.actors.add(new OvalActor(100, 100));
		this.actors.add( new RectangleActor(100, 100));
		this.actors.add(new Shootingstar(100,100));
		this.actors.add(new snowman());
		
		
		for (int i = 0; i < 20; i++) {
			this.actors.add(new Snowflake("big"));
			this.actors.add(new Snowflake("small"));
			this.actors.add(new Snowflake("medium"));
		}
		

	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// TODO Auto-generated method stub
		for (Actor actors : this.actors ) {
			actors.update(gc,delta);
			
		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
